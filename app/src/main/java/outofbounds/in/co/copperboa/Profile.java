package outofbounds.in.co.copperboa;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by sanjitroy on 21/12/16.
 */

public class Profile {

    Identity identity ;
    Credentials credentials ;
    ArrayList<String> bookShelf ;
    ArrayList<String> readBooks ;
    CurrentlyReading currentlyReading ;
    String token ;

    public Profile(String fname, String lname, String email, String googleId, String token){
        identity = new Identity(fname, lname, email) ;
        credentials = new Credentials(googleId) ;
        this.token = token ;
    }

    public Profile(String fname, String lname, String email, String googleId){
        identity = new Identity(fname, lname, email) ;
        credentials = new Credentials(googleId) ;
        token="badvalue" ;
    }

    public String getToken() {
        return token ;
    }

    public String getFirstName(){
        return identity.getFirstName() ;
    }
    public String getLastName(){
        return identity.getLastName() ;
    }
    public String getEmailId(){
        return identity.getEmailId() ;
    }

    public void setBookShelf(ArrayList<String> bookShelf){
        this.bookShelf = bookShelf ;
    }

    public ArrayList<String> getBookShelf() {
        return bookShelf ;
    }


    private class Identity {
        String firstName ;
        String lastName ;
        String emailId ;
        String userName ;

        public Identity(String firstName, String lastName, String emailId){
            this.firstName = firstName ;
            this.lastName = lastName ;
            this.emailId = emailId ;
        }

        public void setFirstName(String firstName){
            this.firstName = firstName ;
        }

        public void setLastName(String lastName){
            this.lastName = lastName ;
        }

        public void setEmailId(String emailId){
            this.emailId = emailId ;
        }

        public void setUserName(String userName){
            this.userName = userName ;
        }

        public String getFirstName(){
            return firstName ;
        }

        public String getLastName(){
            return lastName ;
        }

        public String getEmailId(){
            return emailId ;
        }

        public String getUserName(){
            return userName ;
        }
    }

    private class Credentials {
        String googleId ;

        public Credentials(String googleId){
            this.googleId = googleId ;
        }

        public void setGoogleId(String googleId){
            this.googleId = googleId ;
        }

        public String getGoogleId(){
            return googleId ;
        }
    }

    private class CurrentlyReading {
        String bookId ;
        String bookName ;
        String bookAuthor ;
        int totalPages ;
        int pagesRead ;
    }
}
