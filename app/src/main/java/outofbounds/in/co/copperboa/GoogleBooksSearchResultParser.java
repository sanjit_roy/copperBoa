package outofbounds.in.co.copperboa;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by sanjitroy on 15/12/16.
 */

public class GoogleBooksSearchResultParser {

    public static ArrayList<Book> retrieveResultFromResponse(String response)throws JSONException {
        JSONObject JSONResponse ;
        JSONArray bookArray ;
        JSONObject singleBook ;
        JSONObject bookInfo ;
        String title ;
        //Change this into a Java array
        JSONArray authorArray ;
        String author ;
        String url ;

        ArrayList<Book> bookArrayList = new ArrayList<>() ;

        if(response == null) {
            return new ArrayList<>() ;
        }

        JSONResponse = new JSONObject(response) ;
        if(JSONResponse.has("items")) {
            bookArray = JSONResponse.getJSONArray("items") ;

            for(int i=0; i<bookArray.length(); i++){
                singleBook = bookArray.getJSONObject(i) ;
                url = (singleBook.has("selfLink"))?singleBook.getString("selfLink"):null ;
                bookInfo = singleBook.getJSONObject("volumeInfo") ;
                // Later on just skip books without values.
                title = (bookInfo.has("title"))?bookInfo.getString("title"):"Not Available" ;
                author = (bookInfo.has("publisher"))?bookInfo.getString("publisher"):"Not Available" ;
                Book bookData = new Book(title, author, url) ;
                bookArrayList.add(bookData) ;
            }
        }

        return bookArrayList ;
    }
}
