package outofbounds.in.co.copperboa;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by sanjitroy on 13/12/16.
 */

public class ResultParser {

    public static ArrayList<Book> retieveResultFromResponse(String response) throws JSONException{
        if(response == null) {
            return new ArrayList<>() ;
        }
        ArrayList<Book> bookArray = new ArrayList<>() ;
        JSONObject jsonObject = new JSONObject(response) ;
        jsonObject = jsonObject.getJSONObject("volumeInfo") ;
        String title = "" ;
        String publisher = "" ;
        if(jsonObject.has("title")){
            title = jsonObject.getString("title") ;
        }
        if(jsonObject.has("publisher")){
            publisher = jsonObject.getString("publisher") ;
        }

        Book bookData = new Book(title, publisher, title) ;
        bookArray.add(bookData) ;
        return bookArray ;
    }
}
