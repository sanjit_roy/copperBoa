package outofbounds.in.co.copperboa;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by sanjitroy on 12/12/16.
 */

public class BookSearch extends AsyncTask<String, Void, String> {

    DataDownloadListener mDownloadListener ;

    public BookSearch(DataDownloadListener dcl) {

        mDownloadListener = dcl ;
    }

    @Override
    protected String doInBackground(String... params){
        try{
            return downloadData(params[0]) ;
        }
        catch (IOException e){
            e.printStackTrace();
            return null ;
        }

    }

    @Override
    protected void onPostExecute(String result){
        try {
            mDownloadListener.downloadComplete(ResultParser.retieveResultFromResponse(result));
        }
        catch (JSONException e){
            e.printStackTrace() ;
        }
    }

    private String downloadData(String urlString) throws IOException {
        InputStream is = null ;
        try {
            URL url = new URL(urlString) ;
            HttpURLConnection conn = (HttpURLConnection) url.openConnection() ;
            conn.setRequestMethod("GET") ;
            conn.connect() ;

            //getting the input stream
            is = conn.getInputStream() ;

            return convertToString(is) ;
        }
        finally {
            if(is != null){
                is.close();
            }
        }
    }

    private String convertToString(InputStream is) throws IOException {
        BufferedReader buff = new BufferedReader(new InputStreamReader(is)) ;
        StringBuilder total = new StringBuilder() ;
        String line ;
        while((line = buff.readLine()) != null){
            total.append(line) ;
        }
        return new String(total) ;
    }
}
