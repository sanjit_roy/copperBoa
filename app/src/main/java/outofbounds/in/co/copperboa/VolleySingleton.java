package outofbounds.in.co.copperboa;

import android.util.Log;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by sanjitroy on 22/12/16.
 */

public class VolleySingleton {
    private static VolleySingleton vInstance = null ;
    private RequestQueue mRequestQueue ;
    private VolleySingleton() {
        mRequestQueue = Volley.newRequestQueue(MainApplication.getAppContext()) ;
    }

    public static VolleySingleton getInstance() {
        if(vInstance == null){
            vInstance = new VolleySingleton() ;
            return vInstance ;
        }
        else{
            return vInstance ;
        }
    }

    public RequestQueue getSingleRequestQueue() {
        return mRequestQueue ;
    }
}
