package outofbounds.in.co.copperboa;

import android.*;
import android.Manifest;
import android.app.Activity;
import android.app.PendingIntent;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks ;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener ;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.plus.Plus;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private static final int SIGNED_IN = 0 ;
    private static final int STATE_SIGNED_IN = 1 ;
    private static final int STATE_IN_PROGRESS = 2 ;
    private static final int RC_SIGN_IN = 0 ;
    private static final int APP_PERMISSIONS_REQUEST_GET_ACCOUNTS = 1 ;

    //private GoogleApiClient mGoogleApiClient ;

    //private SignInButton mSignInButton ;
    //private Button mSignOutButton ;
    private TextView mStatus ;
    private Button signcall ;

    private DrawerLayout mDrawerLayout ;

    private SharedPreferences sharedPrefs ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //TODO: Change the nav view for signed users.
        super.onCreate(savedInstanceState) ;
        setContentView(R.layout.activity_main) ;
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar) ;
        setSupportActionBar(toolbar) ;

        //NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_drawer) ;
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer) ;

        ActionBar supportActionBar = getSupportActionBar() ;

        if(supportActionBar != null){
            supportActionBar.setHomeAsUpIndicator(R.drawable.ic_menu_white_24px) ;
            supportActionBar.setDisplayHomeAsUpEnabled(true) ;
        }

        sharedPrefs = getApplicationContext().getSharedPreferences("boadata", Context.MODE_PRIVATE) ;

        initializeNavView() ;

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab) ;
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show() ;
            }
        }) ;
    }

    @Override
    protected void onActivityResult(int requestCode, int responseCode, Intent intent){
        //TODO: Check the resultCode
        initializeNavView() ;
    }

    private void initializeNavView() {
        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_drawer) ;

        View header = navigationView.getHeaderView(0) ;
        TextView navName = (TextView) header.findViewById(R.id.nav_user_name) ;
        String name = sharedPrefs.getString("firstName", "badvalue") ;

        navigationView.getMenu().clear() ;
        if(!name.equals("badvalue")){
            String namesetup = "Hello, " + name ;
            navName.setText(namesetup);
            //This should be the navigation menu.
            navigationView.inflateMenu(R.menu.menu_login) ;
            navigationView.inflateMenu(R.menu.menu_navigation) ;
        }
        else {
            navigationView.inflateMenu(R.menu.menu_login) ;
        }

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.menu_log_in_item:
                        startActivityForResult(new Intent(MainActivity.this, LoginActivity.class), 1) ;
                        mDrawerLayout.closeDrawers();
                        menuItem.setCheckable(false) ;
                        return true ;

                    case R.id.menu_bookshelf_item:
                        startActivity(new Intent(MainActivity.this, BookshelfActivity.class)) ;
                        mDrawerLayout.closeDrawers() ;
                        menuItem.setCheckable(false) ;
                        return true ;

                    case R.id.menu_on_sale_item:
                        startActivity(new Intent(MainActivity.this, BooksOnSaleActivity.class)) ;
                        mDrawerLayout.closeDrawers() ;
                        menuItem.setCheckable(false) ;
                        return true ;

                    default:
                }
                //TODO: Change this into a var
                return true ;
            }

        });

    }


    private void call_sign() {
        Intent intent = new Intent(this, LoginActivity.class) ;
        startActivity(intent);
    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu) ;

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE) ;
        SearchView searchView = (SearchView) menu.findItem(R.id.menu_search).getActionView() ;

        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName())) ;
        searchView.setIconifiedByDefault(true) ;
        searchView.setSubmitButtonEnabled(true) ;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId() ;

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true ;
        }

        else if (id == android.R.id.home) {
            mDrawerLayout.openDrawer(GravityCompat.START) ;
        }

        return super.onOptionsItemSelected(item) ;
    }
}
