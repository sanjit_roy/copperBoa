package outofbounds.in.co.copperboa;

import java.util.ArrayList;

/**
 * Created by sanjitroy on 13/12/16.
 */

public interface DataDownloadListener {
    void downloadComplete(ArrayList<Book> books) ;
}
