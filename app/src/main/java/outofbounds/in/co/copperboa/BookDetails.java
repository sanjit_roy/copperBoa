package outofbounds.in.co.copperboa;

import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONException;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

//TODO: Prevent double addition of books in bookshelf.
public class BookDetails extends AppCompatActivity implements View.OnClickListener{
    TextView book_title ;
    TextView book_author ;
    TextView book_tag ;
    TextView book_price ;
    TextView book_rating ;
    Button buy_button ;
    Button own_this_button ;
    Button sell_this_button ;
    ImageView book_thumb ;

    Book mBook ;
    String token ;


    SharedPreferences boaData ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_details);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        Intent intent = getIntent() ;
        String bookLink = intent.getStringExtra("bookLink") ;

        intializeFields() ;

        volleyBookDetails(bookLink) ;

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.own_this_button:
                addToShelf() ;
                break ;

            case R.id.button_sell_this:
                sellthisbook() ;
                break ;

            case R.id.buy_this_button:
                getBooksOnSale() ;
                break ;

            default:
        }
    }

    private void intializeFields() {
        book_title = (TextView) findViewById(R.id.book_info_name) ;
        book_author  = (TextView) findViewById(R.id.book_info_author) ;
        book_tag = (TextView) findViewById(R.id.book_info_tag) ;
        book_price = (TextView) findViewById(R.id.book_info_price) ;
        book_rating = (TextView) findViewById(R.id.book_info_avgRating) ;

        book_thumb = (ImageView) findViewById(R.id.book_thumb) ;

        own_this_button = (Button) findViewById(R.id.own_this_button) ;
        own_this_button.setOnClickListener(this) ;
        sell_this_button = (Button) findViewById(R.id.button_sell_this) ;
        sell_this_button.setOnClickListener(this) ;
        buy_button = (Button) findViewById(R.id.buy_this_button) ;
        buy_button.setOnClickListener(this) ;

        boaData = getApplicationContext().getSharedPreferences("boadata", Context.MODE_PRIVATE) ;
        token = boaData.getString("token", null) ;
    }

    private void addToShelf() {
        volleyAddToShelf() ;
    }

    private void volleyBookDetails(String url) {
        RequestQueue rQueue = VolleySingleton.getInstance().getSingleRequestQueue() ;

        StringRequest stringReq = new StringRequest(Request.Method.GET, url, new com.android.volley.Response.Listener<String>(){
            @Override
            public void onResponse(String response) {
                try {
                    bookDetailsDownloadComplete(BookDetailsResultParser.retreiveResultsFromResponse(response));
                }
                catch (JSONException e){
                    e.printStackTrace() ;
                }
            }
        }, new com.android.volley.Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){
                error.printStackTrace();
            }
        }) ;

        rQueue.add(stringReq) ;
    }

    private void bookDetailsDownloadComplete(Book book){
        mBook = book ;
        book_title.setText(book.getTitle());
        book_author.setText(book.getPublisher());
        book_tag.setText(book.getId());

        //Log.i("image", mBook.getThumbnail()) ;
        // TODO: Add default images.
        Picasso.with(this)
                .load(mBook.getThumbnail())
                .into(book_thumb) ;
    }

    private void volleyAddToShelf() {
        RequestQueue rQueue = VolleySingleton.getInstance().getSingleRequestQueue() ;
        String url = getResources().getString(R.string.serverUrl)+ "/AddBookToShelf?bookId=" + mBook.getId() ;
        // http://localhost:8080/api/v1/AddBookToShelf?bookId=123456
        StringRequest stringReq = new StringRequest(Request.Method.GET, url, new com.android.volley.Response.Listener<String>(){
            @Override
            public void onResponse(String response) {
                try {
                    saveBookShelfLocally(SuccessParser.retrieveResultFromResponse(response));
                }
                catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }, new com.android.volley.Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>() ;
                params.put("x-access-token", token) ;

                return params ;
            }
        } ;

        rQueue.add(stringReq) ;
    }

    private void saveBookShelfLocally(Boolean success) {
        //TODO: Handle connection flaws here.
        //TODO: Server should check if anything was updated
        //Toast.makeText(this, success.toString(), Toast.LENGTH_SHORT).show();
        //TODO: If expired token call LogIn activity.

        String bookId ;
        String bookTitle ;
        String bookAuthor ;
        String bookPublisher ;
        String bookThumbnail ;
        if(success){
            bookId = mBook.getId() ;
            bookTitle = mBook.getTitle() ;
            bookThumbnail = mBook.getThumbnail() ;

            Set<String> hset = boaData.getStringSet("bookshelf", new HashSet<String>()) ;
            Set<String> eset = new HashSet<String>(hset) ;
            //Toast.makeText(this, hset.size(), Toast.LENGTH_SHORT).show() ;
            Log.i("len",  Integer.toString(hset.size())) ;
            eset.add(mBook.getId()) ;
            boaData.edit().putStringSet("bookshelf", eset).apply() ;

            boaData.edit().putString(bookId+"_title", bookTitle).apply() ;
            boaData.edit().putString(bookId+"_thumbnail", bookThumbnail).apply() ;

            //TODO: Display a success message here.
            Toast.makeText(this, "Succesfully added" + boaData.getString(bookId+"_thumbnail", "badvalue"), Toast.LENGTH_SHORT).show();
        }
        else{
            Toast.makeText(this, "False response received", Toast.LENGTH_SHORT).show();
        }
    }

    private void sellthisbook() {
        startActivity(new Intent(this, SellDetailsActivity.class).putExtra("bookId", mBook.getId()).putExtra("bookThumb", mBook.getThumbnail()));
    }

    private void getBooksOnSale() {

    }
}
