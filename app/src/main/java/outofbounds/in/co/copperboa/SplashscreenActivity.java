package outofbounds.in.co.copperboa;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


/**
 * 1. Check token
 * 2. Download profile
 * 3. App version
 * 4. First time open
 * 5. Remove delay.
 */

public class SplashscreenActivity extends AppCompatActivity {

    SharedPreferences sharedPrefs ;
    RequestQueue requestQueue ;
    String token ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);

        final Handler handler = new Handler() ;
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                initializeAppValues() ;
            }
        }, 1000) ;

        sharedPrefs = DataSingleton.getInstance().getAppSharedPref() ;
        requestQueue = VolleySingleton.getInstance().getSingleRequestQueue() ;

    }

    protected void initializeAppValues() {
        token = sharedPrefs.getString("token", null) ;
        Boolean signedUp = false ;
        if(token != null) {
            refreshToken() ;
            signedUp = true ;
        }
        //Check and refresh token here.
        //Download profiles and checksum for value changes.
        startActivity(new Intent(SplashscreenActivity.this, MainActivity.class).putExtra("signedUp", false)) ;
    }

    private void refreshToken() {
        String url = getResources().getString(R.string.serverUrl)+ "/RefreshToken";
        StringRequest stringReq = new StringRequest(Request.Method.GET, url, new com.android.volley.Response.Listener<String>(){
            @Override
            public void onResponse(String response){
                try {
                    getToken(response) ;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error){
                //TODO: DO something here.
                error.printStackTrace();
                Toast.makeText(getBaseContext(), "Network error", Toast.LENGTH_SHORT).show();
            }
        })
        {
           @Override
           public Map<String, String> getHeaders() throws AuthFailureError {
               Map<String, String> params = new HashMap<String, String>() ;
               params.put("x-access-token", token) ;

               return params ;
           }
        } ;

        stringReq.setShouldCache(false) ;
        requestQueue.add(stringReq) ;
    }

    private void getToken(String response) throws JSONException{
        JSONObject resp = new JSONObject(response) ;
        String force = (resp.has("force"))?resp.getString("force"):null ;
        if(force != null) {
            sharedPrefs.edit().putString("token", force).apply();
            Log.d("Splashscreen", "Refreshed Token") ;
        }
        else {
            //TODO: Send an error to an eventbus
        }

        finish() ;
    }
}
