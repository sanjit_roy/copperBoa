package outofbounds.in.co.copperboa;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by sanjitroy on 29/12/16.
 */

public class DataSingleton {
    private static DataSingleton boaData = null ;
    private SharedPreferences sharedPrefs ;

    private DataSingleton() {
        sharedPrefs = MainApplication.getAppContext().getSharedPreferences("boadata", Context.MODE_PRIVATE) ;
    }

    public static DataSingleton getInstance() {
        if(boaData == null){
            boaData = new DataSingleton() ;
            return boaData ;
        }
        else{
            return boaData ;
        }
    }

    public SharedPreferences getAppSharedPref() {
        return sharedPrefs ;
    }

}
