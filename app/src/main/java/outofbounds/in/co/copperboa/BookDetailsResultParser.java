package outofbounds.in.co.copperboa;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by sanjitroy on 26/12/16.
 */

public class BookDetailsResultParser {

    public static Book retreiveResultsFromResponse(String response)throws JSONException {

        JSONArray items ;

        ArrayList<String> authors ;
        String author ;
        String ISBN10 ;
        String ISBN13 ;
        ArrayList<String> tags ;
        int price ;
        String currencyCode ;

        JSONObject book = new JSONObject(response) ;
        String id = (book.has("id"))?book.getString("id"):null ;
        String url = (book.has("selfLink"))?book.getString("selfLink"):null ;
        JSONObject info = (book.has("volumeInfo"))?book.getJSONObject("volumeInfo"):null ;
        //Check for null values.
        String title = (info.has("title"))?info.getString("title"):null ;
        String publisher = (info.has("publisher"))?info.getString("publisher"):null ;
        String publishedDate = (info.has("publishedDate"))?info.getString("publishedDate"):null ;
        String description = (info.has("description"))?info.getString("description"):null ;
        int pages = (info.has("pageCount"))?info.getInt("pageCount"):-1 ;
        int rating = (info.has("averageRating"))?info.getInt("averageRating"):-1 ;
        int totalRatings = (info.has("totalRatings"))?info.getInt("totalRatings"):-1 ;

        // TODO: Change this image getter.
        JSONObject images = info.getJSONObject("imageLinks") ;
        String thumbnail ;
        if(images.has("thumbnail")){
            thumbnail = images.getString("thumbnail") ;
        }
        else if(images.has("medium")){
            thumbnail = images.getString("medium") ;
        }
        else if(images.has("small")){
            thumbnail = images.getString("small") ;
        }
        else{
            thumbnail = images.getString("smallThumbnail") ;
        }

        Book mbook = new Book(title, publisher, url) ;
        mbook.setId(id) ;
        mbook.setPublishedDate(publishedDate) ;
        mbook.setDescription(description) ;
        mbook.setPages (pages);
        mbook.setRating(rating);
        mbook.setTotalRatings(totalRatings) ;
        mbook.setThumbnail(thumbnail);

        return mbook ;
    }

}
