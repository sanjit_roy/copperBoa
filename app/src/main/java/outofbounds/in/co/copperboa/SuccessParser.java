package outofbounds.in.co.copperboa;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by sanjitroy on 27/12/16.
 */

public class SuccessParser{
    public static Boolean retrieveResultFromResponse(String response) throws JSONException {
        JSONObject result = new JSONObject(response) ;
        String success = (result.has("success"))?result.getString("success"):null ;

        if(success.equals("true")){
            return true ;
        }
        else {
            return false ;
        }


    }
}
