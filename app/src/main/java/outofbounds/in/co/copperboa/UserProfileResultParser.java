package outofbounds.in.co.copperboa;

import android.content.Context;
import android.content.SharedPreferences;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static android.R.attr.hint;

/**
 * Created by sanjitroy on 21/12/16.
 */

public class UserProfileResultParser {
    private static Profile mUser ;
    public static Profile retrieveResultFromResponse(String response) {
        // Forgot to store token
        //TODO: Store token
        //TODO: Make sure that there is a controlled recursion between signup and signin.
        try {
            JSONObject JSONResult = new JSONObject(response) ;

            String firstName= null , lastName= null, emailId= null, userName= null, googleId= null ;
            String token = null ;
            Boolean hasSignedUp = true ;
            if(JSONResult.has("success")){
                Boolean success = JSONResult.getBoolean("success") ;
                if(!success){
                    return null ;
                }

                if(JSONResult.has("force") && !JSONResult.has("hint")) {
                    token = JSONResult.getString("force") ;
                }
                else {
                    hasSignedUp = false ;
                }

                if(JSONResult.has("data")) {
                    JSONObject data = JSONResult.getJSONObject("data") ;
                    if(data.has("identity")) {
                        JSONObject identity = data.getJSONObject("identity") ;
                        firstName = identity.getString("firstName") ;
                        lastName = identity.getString("lastName") ;
                        emailId = identity.getString("emailId") ;
                        userName = identity.getString("userName") ;

                        if(data.has("credentials")){
                            JSONObject credentials = data.getJSONObject("credentials") ;
                            googleId = credentials.getString("googleId") ;
                        }
                    }
                    else{
                        return null ;
                    }
                }
                else {
                    return null ;
                }
            }

            if(hasSignedUp){
                mUser = new Profile(firstName, lastName, emailId, googleId, token) ;
            }
            else {
                mUser = new Profile(firstName, lastName, emailId, googleId) ;
            }

            return mUser ;

        }
        catch (JSONException e){
            e.printStackTrace() ;
        }

        return mUser ;
    }
}
