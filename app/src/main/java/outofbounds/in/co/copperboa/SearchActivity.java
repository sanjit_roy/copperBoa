package outofbounds.in.co.copperboa;

import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;

import java.util.ArrayList;

public class SearchActivity extends AppCompatActivity implements DataDownloadListener{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState) ;
        setContentView(R.layout.activity_search) ;

        String query = "Search";
        String url = "https://www.googleapis.com/books/v1/volumes?q=" ;

        Intent intent = getIntent() ;
        if(Intent.ACTION_SEARCH.equals(intent.getAction())) {
            query = intent.getStringExtra(SearchManager.QUERY) ;
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar) ;
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar() ;
        if(actionBar != null){
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(query);
        }

        String q = query.replace(" ","+") ;
        url = url.concat(q) ;


        //RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.search_list_view) ;
        //ContentAdapter contentAdapter = new ContentAdapter() ;
        //mRecyclerView.setAdapter(contentAdapter) ;

        //mRecyclerView.setLayoutManager(new LinearLayoutManager(this)) ;

        if(isNetworkConnected()){
            // new BookSearch(this).execute("https://www.googleapis.com/books/v1/volumes/grccDzrTWLUC") ;
            //makeVolleyRequest("https://www.googleapis.com/books/v1/volumes?q=god+delusion") ;
            makeVolleyRequest(url) ;
        }
        else{
            new AlertDialog.Builder(this).setTitle("Error").setMessage("Internet off asshole").setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener(){
                public void onClick(DialogInterface dialog, int which){

                }
            }).setIcon(android.R.drawable.ic_dialog_alert).show() ;
        }

    }


    private void makeVolleyRequest(String url){
        RequestQueue rQueue = VolleySingleton.getInstance().getSingleRequestQueue() ;

        StringRequest stringReq = new StringRequest(Request.Method.GET, url, new com.android.volley.Response.Listener<String>(){

            @Override
            public void onResponse(String response) {
                try {
                    downloadComplete(GoogleBooksSearchResultParser.retrieveResultFromResponse(response));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        }) ;

        rQueue.add(stringReq) ;
    }

    @Override
    public void downloadComplete(ArrayList<Book> books){
        // Received books here.
        Log.i("Msg", "I should be here.") ;

        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.search_list_view) ;
        ContentAdapter contentAdapter = new ContentAdapter(mRecyclerView.getContext(), books) ;
        mRecyclerView.setAdapter(contentAdapter) ;

        mRecyclerView.setLayoutManager(new LinearLayoutManager(this)) ;

    }

    private boolean isNetworkConnected() {
        ConnectivityManager connMng = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE) ;
        NetworkInfo netInfo = connMng.getActiveNetworkInfo() ;
        return netInfo != null && netInfo.isConnected() ;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView author ;
        public TextView title ;

        public ViewHolder(LayoutInflater inflater, ViewGroup parent){
            super(inflater.inflate(R.layout.book_list, parent, false)) ;

            author = (TextView) itemView.findViewById(R.id.book_author) ;
            title = (TextView) itemView.findViewById(R.id.book_title) ;
        }
    }

    public static class ContentAdapter extends RecyclerView.Adapter<ViewHolder> {
        private static final int LENGTH = 18 ;
        private String[] mAuthors ;
        private String[] mTitles ;
        private String[] mUrl ;
        Context context ;

        public ContentAdapter(Context context, ArrayList<Book> books){
            mAuthors = new String[books.size()] ;
            mTitles = new String[books.size()] ;
            mUrl = new String[books.size()] ;
            this.context = context ;
            for(int i=0; i<books.size(); i++) {
                mTitles[i] = books.get(i).getTitle() ;
                mAuthors[i] = books.get(i).getAuthor() ;
                mUrl[i] = books.get(i).getUrl() ;
            }
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
            return new ViewHolder(LayoutInflater.from(parent.getContext()), parent) ;
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, final int position){
            holder.title.setText(mTitles[position % mTitles.length]) ;
            holder.author.setText(mAuthors[position % mAuthors.length]);

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, BookDetails.class) ;
                    intent.putExtra("bookLink", mUrl[position % mUrl.length]) ;

                    context.startActivity(intent) ;
                }
            });
        }

        @Override
        public int getItemCount(){
            return LENGTH ;
        }
    }
}
