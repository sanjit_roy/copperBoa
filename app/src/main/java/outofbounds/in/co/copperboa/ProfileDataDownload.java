package outofbounds.in.co.copperboa;

/**
 * Created by sanjitroy on 21/12/16.
 */

public interface ProfileDataDownload {
    void onDataDownloaded() ;
}
