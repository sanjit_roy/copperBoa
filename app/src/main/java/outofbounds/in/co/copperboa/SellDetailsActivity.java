package outofbounds.in.co.copperboa;

import android.app.SearchManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONException;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class SellDetailsActivity extends AppCompatActivity implements View.OnClickListener{
    Button submit_action ;
    Button get_places ;
    EditText price ;
    TextView listedPrice ;
    TextView address ;
    double longitude ;
    double latitude ;

    SharedPreferences sharedPrefs ;
    RequestQueue rQueue ;

    private String bookId ;
    private String token ;
    private String thumbnail ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sell_details);

        Intent intent = getIntent() ;

        bookId = (intent.hasExtra("bookId"))?intent.getStringExtra("bookId"):null ;
        thumbnail = (intent.hasExtra("bookThumb"))?intent.getStringExtra("bookThumb"):null ;

        rQueue = VolleySingleton.getInstance().getSingleRequestQueue() ;
        sharedPrefs = DataSingleton.getInstance().getAppSharedPref() ;
        token = sharedPrefs.getString("token", "badvalue") ;
        initializeViews() ;
    }

    private void initializeViews() {
        submit_action = (Button) findViewById(R.id.button_confirm_selling_price) ;
        price = (EditText) findViewById(R.id.edit_text_book_price) ;
        listedPrice = (TextView) findViewById(R.id.text_listed_price) ;
        get_places = (Button) findViewById(R.id.button_google_places) ;
        address = (TextView) findViewById(R.id.text_places_content) ;

        submit_action.setOnClickListener(this) ;
        get_places.setOnClickListener(this) ;

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_confirm_selling_price:
                putBookOnSale() ;
                break ;

            case R.id.button_google_places:
                findAddress(view);
                break ;

            default:
        }
    }

    public void findAddress(View view){
        try {
            Intent intent = new PlaceAutocomplete
                    .IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                    .build(this) ;
            startActivityForResult(intent, 1);
        }
        catch (Exception e){
            // TODO: Handle the following errors: 1. GooglePlayServicesRepairable 2. GooglePlayServicesNotAvailable
        }
    }

    protected void onActivityResult(int requestCode, int responseCode, Intent data) {
        if(requestCode == 1) {
            if(responseCode == RESULT_OK) {
                Place places = PlaceAutocomplete.getPlace(this, data) ;
                longitude = places.getLatLng().longitude ;
                latitude = places.getLatLng().latitude ;
                address.setText(places.getAddress() + " and " + places.getLatLng().toString()) ;
            }
            else if(responseCode == PlaceAutocomplete.RESULT_ERROR) {
                address.setText("Holy Crap, Error") ;
            }
            else if(responseCode == RESULT_CANCELED) {
                address.setText("You are being a very naughty boy, Jose") ;
            }
            else {
                address.setText("Congrats Google, you have finally broken a good man");
            }
        }
    }

    private void putBookOnSale() {
        //TODO: Activate only if every field is filled.
        final String selfLink = "https://www.googleapis.com/books/v1/volumes/" + bookId ;
        final String editedPrice = price.getText().toString() ;
        Toast.makeText(this, selfLink, Toast.LENGTH_SHORT).show() ;
        Log.d("price", editedPrice) ;

        //token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI1ODVhMTgyZmQ4ZGUxMjBkYjVlMzdkYTMiLCJtYWlsSWQiOiJzYW5qaXRyb3kxM0BnbWFpbC5jb20iLCJnb29nbGVJZCI6IjExMjkxMTQwMDQ4MDA0OTY4MjM2MyIsImlhdCI6MTQ4MzE1MzQyOSwiZXhwIjoxNDgzNTEzNDI5fQ.iPS2zfGjB9dRHYmMshoqe5aVn2tKhF8irzlCf23wrqg" ;

        String url = getResources().getString(R.string.serverUrl)+"/AddBookOnSale" ;

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new com.android.volley.Response.Listener<String>(){
            @Override
            public void onResponse(String response){
                try{
                    onPuttingBookOnSale(SuccessParser.retrieveResultFromResponse(response)) ;
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new com.android.volley.Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){
                //TODO: Remember to end activity here.
                //TODO: Handle volley error here.
                Log.i("Error", "error in sign in volley") ;
            }
        })
        {
           @Override
            protected Map<String, String> getParams() {
               Map<String, String> params = new HashMap<String, String>() ;
               params.put("bookId", bookId) ;
               params.put("selfUrl", selfLink) ;
               params.put("price", editedPrice) ;
               params.put("lon", Double.toString(longitude)) ;
               params.put("lat", Double.toString(latitude)) ;

               return params ;
           }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError{
                Map<String, String> headers = new HashMap<String, String>() ;
                headers.put("x-access-token", token) ;

                return headers ;
            }
        } ;

        rQueue.add(stringRequest) ;
    }


    private void onPuttingBookOnSale(Boolean success) {
        Log.d("location", "putbookonsale") ;
        if(success){
            addToLocalSoldList() ;
        }
        else {
            Toast.makeText(this, "Unsuccessful", Toast.LENGTH_SHORT).show();
        }
    }

    private void addToLocalSoldList() {
        Set<String> hset = sharedPrefs.getStringSet("listedBooks", new HashSet<String>()) ;
        Set<String> eset = new HashSet<String>(hset) ;
        eset.add(bookId) ;
        sharedPrefs.edit().putStringSet("listedBooks", eset).apply() ;
        Toast.makeText(this, "Successful", Toast.LENGTH_SHORT).show();
    }
}
