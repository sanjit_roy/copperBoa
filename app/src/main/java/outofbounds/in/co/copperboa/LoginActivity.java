package outofbounds.in.co.copperboa;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONException;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, View.OnClickListener{
    private GoogleApiClient mGoogleApiClient ;
    private SignInButton mSignInButton ;
    int resultCode ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mSignInButton = (SignInButton) findViewById(R.id.sign_in_button) ;
        mSignInButton.setOnClickListener(this) ;

        GoogleSignInOptions signInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build() ;

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, signInOptions)
                .build() ;

    }


    public void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient) ;
        startActivityForResult(signInIntent, 9001) ;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.sign_in_button:
                signIn() ;
                break ;

            default:
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
            //TODO: Add this method
    }

    @Override
    public void onActivityResult(int requestCode, int responseCode, Intent data){
        super.onActivityResult(requestCode, responseCode, data) ;

        if(requestCode == 9001){
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data) ;
            handleSignInResult(result) ;
        }
    }

    public void handleSignInResult(GoogleSignInResult result){
        if(result.isSuccess()){
            Log.i("net", "success") ;
            GoogleSignInAccount account = result.getSignInAccount() ;
            VolleySignIn(account) ;
        }
    }

    public void VolleySignIn(final GoogleSignInAccount account) {
        RequestQueue mRequestQueue = VolleySingleton.getInstance().getSingleRequestQueue() ;
        String url = getResources().getString(R.string.serverUrl) + "/GoogleSignIn" ;
        StringRequest signInRequest = new StringRequest(Request.Method.POST, url, new com.android.volley.Response.Listener<String>(){
            @Override
            public void onResponse(String response){
                Log.i("auth", "response got") ;
                try {
                    onProfileDownload(UserProfileResultParser.retrieveResultFromResponse(response), account) ;
                }
                catch (Exception e){
                    e.printStackTrace();
                }
            }
        }, new com.android.volley.Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){
                //TODO: Remember to end activity here.
                //TODO: Handle volley error here.
                Log.i("Error", "error in sign in volley") ;
            }
        })
        {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>() ;
                params.put("emailId", account.getEmail()) ;
                params.put("firstName", account.getDisplayName() ) ;
                params.put("lastName", account.getDisplayName()) ;
                params.put("googleId", account.getId()) ;

                return params ;

            }
        } ;

        mRequestQueue.add(signInRequest) ;

    }

    public void onProfileDownload(Profile userProfile, final GoogleSignInAccount account){
        //TODO: Send user back from where he came and complete that task
        /**
         * 1. Check token
         * 2. If bad value call sign in again
         * 3. If !badvalue store it in prefs
         */
        Log.d("auth", "on profile download") ;
        String token = userProfile.getToken() ;

        if(token.equals("badvalue")){
            VolleySignIn(account);
            resultCode = 0 ;
        }
        else{
            //TODO: Make values global
            SharedPreferences sharedPref = getApplicationContext().getSharedPreferences("boadata", Context.MODE_PRIVATE) ;
            SharedPreferences.Editor sprefeditor = sharedPref.edit() ;

            sprefeditor.putString("token", token) ;
            sprefeditor.apply() ;
            storeProfileValues(sharedPref, sprefeditor, userProfile) ;

            resultCode = 1 ;
            setResult(resultCode) ;
            finish() ;
        }


    }

    public void storeProfileValues(SharedPreferences sharedPref, SharedPreferences.Editor sprefeditor, Profile userProfile){
        sprefeditor.putString("firstName", userProfile.getFirstName() ) ;
        sprefeditor.putString("lastName", userProfile.getLastName()) ;
        sprefeditor.putString("emailId", userProfile.getEmailId()) ;
        sprefeditor.commit() ;
    }
}
