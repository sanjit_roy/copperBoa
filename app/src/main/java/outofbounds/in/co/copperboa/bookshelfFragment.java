package outofbounds.in.co.copperboa;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.HashSet;
import java.util.Set;


/**
 * A simple {@link Fragment} subclass.
 */

//TODO: Simulate refresh when fragment reloads.
public class bookshelfFragment extends Fragment {
    SharedPreferences sharedPref ;

    protected enum LayoutManagerType {
        GRID_LAYOUT_MANAGER ,
        LINEAR_LAYOUT_MANAGER
    }

    protected RecyclerView mRecyclerView ;
    protected RecyclerView.LayoutManager recyclerLayoutManager ;
    protected LayoutManagerType mCurrentLayoutManagerType ;

    public bookshelfFragment() {
        // Required empty public constructor
        sharedPref = DataSingleton.getInstance().getAppSharedPref() ;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_bookshelf, container, false) ;
        //TODO: If bookshelf is null then don't initializze recycler view.
        startRecycler(rootView);

        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_bookshelf, container, false) ;
        return rootView ;
    }

    public void startRecycler(View rootView) {
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.horizontal_recycle_view) ;
        //recyclerLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false) ;
        recyclerLayoutManager = new GridLayoutManager(getActivity(), 4) ;
        //mCurrentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER ;

        mRecyclerView.setLayoutManager(recyclerLayoutManager) ;
        ContentAdapter adapter = new ContentAdapter(mRecyclerView.getContext()) ;
        mRecyclerView.setAdapter(adapter) ;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView thumb ;

        public ViewHolder(LayoutInflater inflater, ViewGroup parent){
            super(inflater.inflate(R.layout.book_card, parent, false)) ;
            thumb = (ImageView) itemView.findViewById(R.id.book_thumb) ;
        }
    }

    public static class ContentAdapter extends RecyclerView.Adapter<ViewHolder> {
        private static int LENGTH = 10 ;
        SharedPreferences sharedPrefs ;
        Context context ;
        Set<String> sp_bookshelf ;
        String[] bookshelf ;

        public ContentAdapter(Context context){
            this.context = context ;
            sharedPrefs = DataSingleton.getInstance().getAppSharedPref() ;
            sp_bookshelf = sharedPrefs.getStringSet("bookshelf", new HashSet<String>()) ;
            bookshelf = sp_bookshelf.toArray(new String[sp_bookshelf.size()]) ;
            if(bookshelf.length<10){
                LENGTH = bookshelf.length ;
            }
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
            return new ViewHolder(LayoutInflater.from(parent.getContext()), parent) ;
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position){
            //holder.title.setText(bookshelf[position % bookshelf.length]) ;
            String bookId = bookshelf[position % bookshelf.length] ;
            final String bookLink = "https://www.googleapis.com/books/v1/volumes/" + bookId ;
            String imageUrl = sharedPrefs.getString(bookId+"_thumbnail", "https://unsplash.it/g/128/200") ;

            Picasso.with(context)
                    .load(imageUrl)
                    .into(holder.thumb) ;

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view){
                    Intent intent = new Intent(context, BookDetails.class) ;
                    intent.putExtra("bookLink", bookLink) ;

                    context.startActivity(intent);
                }
            });
        }

        @Override
        public int getItemCount(){
            return LENGTH ;
        }
    }

}
