package outofbounds.in.co.copperboa;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

/**
 * Created by sanjitroy on 22/12/16.
 */

public class MainApplication extends Application {
    private static MainApplication mInstance ;
    @Override
    public void onCreate(){
        super.onCreate();
        mInstance = this ;
    }

    public static MainApplication getInstance() {
        return mInstance ;
    }

    public static Context getAppContext() {
        return mInstance.getApplicationContext() ;
    }

}
