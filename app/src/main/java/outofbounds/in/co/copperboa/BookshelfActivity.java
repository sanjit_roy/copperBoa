package outofbounds.in.co.copperboa;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Set;

public class BookshelfActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bookshelf);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar) ;
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar() ;
        if(actionBar != null){
            actionBar.setDisplayHomeAsUpEnabled(true) ;
            actionBar.setTitle("Your BookShelf") ;
        }

        // TODO:Check if there is network

        requestBookshelfData() ;


        //RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.bookshelf_recyclerview) ;
        // Add content adapters
    }

    private void requestBookshelfData() {
        SharedPreferences sharedPrefs = DataSingleton.getInstance().getAppSharedPref() ;
        Set<String> bookshelfId = sharedPrefs.getStringSet("bookshelf", null) ;
        RecyclerView mRecycler ;

        if(bookshelfId == null){
            Toast.makeText(this , "No data", Toast.LENGTH_SHORT).show();
        }
        else {
            mRecycler = (RecyclerView) findViewById(R.id.bookshelf_recyclerview) ;
            ContentAdapter contentAdapter = new ContentAdapter(mRecycler.getContext(), bookshelfId) ;
            mRecycler.setAdapter(contentAdapter);

            mRecycler.setLayoutManager(new LinearLayoutManager(this));

        }
    }



    //TODO: Change ViewHolder reference.
    public static class ViewHolder extends RecyclerView.ViewHolder {
        //public TextView author ;
        public TextView title ;

        public ViewHolder(LayoutInflater inflater, ViewGroup parent){
            super(inflater.inflate(R.layout.book_list, parent, false)) ;

            //author = (TextView) itemView.findViewById(R.id.book_author) ;
            title = (TextView) itemView.findViewById(R.id.book_title) ;
        }
    }


    public static class ContentAdapter extends RecyclerView.Adapter<SearchActivity.ViewHolder> {
        private static int LENGTH = 18 ;
        private String[] mBookId ;
        Context context ;

        public ContentAdapter(Context context, Set<String> bookShelf){
            this.context = context ;

            mBookId = new String[bookShelf.size()] ;
            LENGTH = bookShelf.size() ;
            int i = 0 ;

            for(String item: bookShelf) {
                mBookId[i] = item ;
                i++ ;
            }

        }

        @Override
        public SearchActivity.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
            return new SearchActivity.ViewHolder(LayoutInflater.from(parent.getContext()), parent) ;
        }

        @Override
        public void onBindViewHolder(SearchActivity.ViewHolder holder, final int position){
            holder.title.setText(mBookId[position % mBookId.length]) ;
            final String url = "https://www.googleapis.com/books/v1/volumes/" + mBookId[position % mBookId.length] ;
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, BookDetails.class) ;
                    intent.putExtra("bookLink", url) ;

                    context.startActivity(intent) ;
                }
            }) ;
        }

        @Override
        public int getItemCount(){
            return LENGTH ;
        }
    }

}
