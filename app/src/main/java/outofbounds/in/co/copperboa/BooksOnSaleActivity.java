package outofbounds.in.co.copperboa;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Set;

public class BooksOnSaleActivity extends AppCompatActivity {
    SharedPreferences sharedPrefs ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_books_on_sale) ;


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar) ;
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar() ;
        if(actionBar != null){
            actionBar.setDisplayHomeAsUpEnabled(true) ;
            actionBar.setTitle("Your BookShelf") ;
        }

        // TODO:Check if there is network
        sharedPrefs = DataSingleton.getInstance().getAppSharedPref() ;

        requestBooksOnSaleData() ;
    }

    private void requestBooksOnSaleData() {
        SharedPreferences sharedPrefs = DataSingleton.getInstance().getAppSharedPref() ;
        Set<String> booksId = sharedPrefs.getStringSet("listedBooks", null) ;
        RecyclerView mRecycler ;

        if(booksId == null){
            Toast.makeText(this , "No data", Toast.LENGTH_SHORT).show();
        }
        else {
            mRecycler = (RecyclerView) findViewById(R.id.recyclerView_booksOnSale) ;
            ContentAdapter contentAdapter = new ContentAdapter(mRecycler.getContext(), booksId) ;
            mRecycler.setAdapter(contentAdapter);

            mRecycler.setLayoutManager(new LinearLayoutManager(this));

        }
    }




    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView author ;
        public TextView title ;

        public ViewHolder(LayoutInflater inflater, ViewGroup parent){
            super(inflater.inflate(R.layout.book_list, parent, false)) ;

            author = (TextView) itemView.findViewById(R.id.book_author) ;
            title = (TextView) itemView.findViewById(R.id.book_title) ;
        }
    }


    public static class ContentAdapter extends RecyclerView.Adapter<ViewHolder> {
        private static int LENGTH = 18 ;
        private String[] mBookId ;
        private String[] mBookTitles ;
        Context context ;
        SharedPreferences sharedPrefs ;

        public ContentAdapter(Context context, Set<String> booksId){
            this.context = context ;
            sharedPrefs = DataSingleton.getInstance().getAppSharedPref() ;
            //TODO: Change this to one line, remove loop.
            mBookId = new String[booksId.size()] ;
            LENGTH = booksId.size() ;
            int i = 0 ;

            for(String item: booksId) {
                mBookId[i] = item ;
                i++ ;
            }

        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
            return new ViewHolder(LayoutInflater.from(parent.getContext()), parent) ;
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, final int position){
            holder.author.setText(mBookId[position % mBookId.length]) ;
            String title = mBookId[position % mBookId.length]+"_title" ;
            holder.title.setText(sharedPrefs.getString(title, "badvalue")) ;
            final String url = "https://www.googleapis.com/books/v1/volumes/" + mBookId[position % mBookId.length] ;
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                //TODO: Change this to a edit offer activity.
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, BookDetails.class) ;
                    intent.putExtra("bookLink", url) ;

                    context.startActivity(intent) ;
                }
            }) ;
        }

        @Override
        public int getItemCount(){
            return LENGTH ;
        }
    }

}
